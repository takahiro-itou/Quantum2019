
namespace  Solution  {

open  Microsoft.Quantum.Primitive;
open  Microsoft.Quantum.Canon;

operation  Solve(x : Qubit[], y : Qubit) : Unit {
    body (...)
    {
        let N = Length(x);
        let ones = (1 <<< N) - 1;
        for ( i in 0..2..(N - 1) ) {
            X(x[i]);
        }
        (ControlledOnInt(ones, X))(x, y);
        (ControlledOnInt(   0, X))(x, y);
        for ( i in 0..2..(N - 1) ) {
            X(x[i]);
        }
    }
    adjoint auto;
}

}
