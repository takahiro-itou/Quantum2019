
namespace  Solution  {

open  Microsoft.Quantum.Primitive;
open  Microsoft.Quantum.Canon;

operation  Solve(qs: Qubit[]) : Unit {
    body (...) {
        let N = Length(qs);
        if ( N == 1 ) {
        } else {
            (ControlledOnInt(0, Solve))([qs[N-1]], qs[0..N-2]);
            for ( i in 0 .. (N-2) ) {
                Controlled H([qs[N-1]], qs[i]);
            }
        }
    }
    controlled auto;
    adjoint auto;
}

}
